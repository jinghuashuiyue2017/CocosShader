# CocosShader

#### 介绍
cocos creator2.0 自定义shader组件

#### 模块
- BaseShader.js：shader定义文件
- NewMaterial.js：材质类
- ShaderComponent.js：shader组件，挂到有渲染的node上面，指定shader定义文件就可以用了

#### 说明
shader定义文件里可以定义任意着色器需要的参数变量，通过材质对象的setUniform和setDefine方法可以随时修改变量值