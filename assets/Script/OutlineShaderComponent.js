const math = cc.vmath;
let ShaderComponent = require('ShaderComponent');

cc.Class({
    extends: ShaderComponent,

    properties: {
        outlineColor: new cc.Color(255,0,0,1),
        outlineSize: new cc.Vec2(0.1, 0.01)
    },

    start () {
        if (this.target) {
            this._super();

            this._material.setUniform("outlineSize", math.vec2.new(this.outlineSize.x, this.outlineSize.y));
            this._material.setUniform("outlineColor", math.vec3.new(this.outlineColor.r/255, this.outlineColor.g/255, this.outlineColor.b/255));
        }
    },

});
