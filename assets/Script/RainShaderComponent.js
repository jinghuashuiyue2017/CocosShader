const math = cc.vmath;
let ShaderComponent = require('ShaderComponent');

cc.Class({
    extends: ShaderComponent,


    // onLoad () {
    //     this._super();
    // },

    start () {
        if (this.target) {
            this._super();
            let texture = this.target.spriteFrame.getTexture();
            this._material.setUniform("texSize", math.vec2.new(texture.width,texture.height));
            this._material.setUniform("iResolution", math.vec3.new(this.target.node.width,this.target.node.height,0));
            
            this._start = Date.now();
        }
    },

    update (dt) {
        let now = Date.now();
        let time = (now - this._start) / 1000;
        this._material.setUniform('iTime', time);
    },
});
