const math = cc.vmath;
const renderEngine = cc.renderer.renderEngine;
const renderer = renderEngine.renderer;

let shader = {
    uniforms: [
        { name: 'texture', type: renderer.PARAM_TEXTURE_2D },
        { name: 'outlineColor', type: renderer.PARAM_FLOAT3, value:math.vec3.new(1,0,0) },
        { name: 'outlineSize', type: renderer.PARAM_FLOAT2, value:math.vec3.new(0.1, 0.01) }
    ],

    pass: [{
        name: 'outline',

        frag:
        `
        uniform sampler2D texture;
        varying vec2 uv0;
        uniform vec2 outlineSize;
        uniform vec3 outlineColor;
        
        int getIsStrokeWithAngel(float cosangel, float sinangel)
        {
            int stroke = 0;
            float a = texture2D(texture, vec2(uv0.x + outlineSize.x * cosangel, uv0.y + outlineSize.y * sinangel)).a; // 这句比较难懂，outlineSize * cos(rad)可以理解为在x轴上投影，除以textureSize.x是因为texture2D接收的是一个0~1的纹理坐标，而不是像素坐标
            float num = 1.0;
            if (a >= num)
            {
                stroke = 1;
            }
            return stroke;
        }
        
        void main()
        {
            vec4 myC = texture2D(texture, vec2(uv0.x, uv0.y));
            
            float num = 1.0;
            if (myC.a >= num)
            {
                gl_FragColor = myC;
                return;
            }
            int strokeCount = 0;
            strokeCount += getIsStrokeWithAngel( 1.0, 0.0);
            strokeCount += getIsStrokeWithAngel( 0.9659258262, 0.2588190451);
            strokeCount += getIsStrokeWithAngel( 0.8660254037, 0.5);
            strokeCount += getIsStrokeWithAngel( 0.7071067811, 0.7071067811);
            strokeCount += getIsStrokeWithAngel( 0.5, 0.8660254037);
            strokeCount += getIsStrokeWithAngel( 0.2588190451, 0.9659258262);
            strokeCount += getIsStrokeWithAngel( 0.0, 1.0);
            strokeCount += getIsStrokeWithAngel(-0.2588190451, 0.9659258262);
            strokeCount += getIsStrokeWithAngel(-0.5, 0.8660254037);
            strokeCount += getIsStrokeWithAngel(-0.7071067811, 0.7071067811);
            strokeCount += getIsStrokeWithAngel(-0.8660254037, 0.5);
            strokeCount += getIsStrokeWithAngel(-0.9659258262, 0.2588190451);
            strokeCount += getIsStrokeWithAngel(-1.0, 0.0);
            strokeCount += getIsStrokeWithAngel(-0.9659258262, -0.2588190451);
            strokeCount += getIsStrokeWithAngel(-0.8660254037, -0.5);
            strokeCount += getIsStrokeWithAngel(-0.7071067811, -0.7071067811);
            strokeCount += getIsStrokeWithAngel(-0.5, -0.8660254037);
            strokeCount += getIsStrokeWithAngel(-0.2588190451, -0.9659258262);
            strokeCount += getIsStrokeWithAngel(0.0, -1.0);
            strokeCount += getIsStrokeWithAngel(0.2588190451, -0.9659258262);
            strokeCount += getIsStrokeWithAngel(0.5, -0.8660254037);
            strokeCount += getIsStrokeWithAngel(0.7071067811, -0.7071067811);
            strokeCount += getIsStrokeWithAngel(0.8660254037, -0.5);
            strokeCount += getIsStrokeWithAngel(0.9659258262, -0.2588190451);
        
            if (strokeCount > 0)
            {
                myC.rgb = outlineColor;
                myC.a = 1.0;
            }
        
            gl_FragColor = myC;
        }
        `,
    }]
};

module.exports = shader;