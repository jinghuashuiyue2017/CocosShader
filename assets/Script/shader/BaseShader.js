// const math = cc.vmath;
const renderEngine = cc.renderer.renderEngine;
const renderer = renderEngine.renderer;
const gfx = renderEngine.gfx;

let shader = {
    uniforms: [{ name: 'texture', type: renderer.PARAM_TEXTURE_2D }],

    defines: [],

    pass: [{
        name: 'base',

        cullMode: gfx.CULL_NONE,
        depth: {
            depthTest: false,
            depthWrite: false
        },
        blend: {
            rbgBlendMode: gfx.BLEND_FUNC_ADD,
            srcRBGBlendFactor: gfx.BLEND_SRC_ALPHA,
            dstRBGBlendFactor: gfx.BLEND_ONE_MINUS_SRC_ALPHA,
            alphaBlendMode: gfx.BLEND_FUNC_ADD,
            srcAlphaBlendFactor: gfx.BLEND_SRC_ALPHA,
            dstAlphaBlendFactor: gfx.BLEND_ONE_MINUS_SRC_ALPHA
        },

        vert: 
        `
        uniform mat4 viewProj;
        uniform mat4 model;
        attribute vec3 a_position;
        attribute vec2 a_uv0;
        varying vec2 uv0;
        void main () {
            mat4 mvp;
            mvp = viewProj * model;
        
            vec4 pos = mvp * vec4(a_position, 1);
            gl_Position = pos;
            uv0 = a_uv0;
        }
        `,

        frag:
        `
        uniform sampler2D texture;
        varying vec2 uv0;
        void main () {
            vec4 c = texture2D(texture, uv0);
            gl_FragColor = c;
        }
        `,
    }]
};

module.exports = shader;